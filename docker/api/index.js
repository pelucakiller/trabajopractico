const express = require('express');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const app = express();

const httpPort = 8080;
const secretJWT = '9esdfdssdfsfsfsf';   // TODO: tomar de archivo no versionable

function findAutenticacionById(id){
  return autenticacion.find(function(item) {
     return item.id == id
  });

}

var corsOptions = {
  origin: ['https://www.techu.example'],
  allowedHeaders: ['Content-Type', 'Autorization']
}

/******************************************************************************
 *
 * MIDDLEWARE
 * 
 *****************************************************************************/
app.use(cors(corsOptions));

app.use(function(req,res,next) {
  console.log("###################################");
  console.log("########" + req.url);
  console.log("###################################");
  next();
});

app.use(function(req,res,next) {
  req.autentication = false;
  if ( req.headers['autorization']) {
     try {
       var decoded = jwt.verify(req.headers['autorization'], secretJWT);   
       req.autenticacion = decoded.data;
     } catch (e) {
       console.log("jwt failure");
     }
  }
  next();
});
app.use(express.json());

/******************************************************************************
 *
 * FUNCTIONS
 * 
 *****************************************************************************/


function nextId(obj) { 
  return parseInt(obj.reduce( (a,b) => obj[a.id] > obj[b.id] ? a.id: b.id),10) + 1;
}

function perfiladas() {
  return personas.map(
    function(persona,idx) {
      return {
        id:persona.id,
        nombre: persona.nombre,
        apellido:persona.apellido,
        perfil:autenticacion[idx].perfil
      };
    }
  );
}

/******************************************************************************
 *
 * ENDPOINTS
 * 
 *****************************************************************************/

app.get('/admin/perfiles', function(req,res) {
  if (req.autenticacion.perfil == 'admin')
    return res.send(Object.values(perfiladas()));
  else
    res.sendStatus(401).send();
});


app.get('/personas', function(req,res) {
  if (req.autenticacion.perfil == 'admin' || req.autenticacion.perfil == 'usuario') 
    return res.send(Object.values(personas))
  else
    res.sendStatus(401).send();
});


app.get('/personas/:id', function(req,res) {
  if (req.autenticacion.perfil == 'admin' || req.autenticacion.id == req.params.id)
    return res.send(personas[req.params.id])
  else
    res.sendStatus(401).send();
});


app.post('/personas/login', function(req,res){
  var nombre = req.body.Nombre;
  var clave = req.body.Clave;

  var persona = personas.find(function(item) {
     return (item.nombre == nombre);
  });

  if (persona) {
    var personaEnSistema = autenticacion.find( (auth) => auth.id == persona.id);
    if (personaEnSistema && personaEnSistema.clave == clave) {
      var data = {id:personaEnSistema.id,perfil:personaEnSistema.perfil};
      var token = jwt.sign({data:data}, secretJWT, {expiresIn: '2m'});
      return res.send('{"token":"' + token + '"}');
    }
  } 
  res.sendStatus(401).send();
});

app.post('/personas', function(req,res){
  var id = nextId(personas);
  var nombre = req.body.Nombre;
  var apellido = req.body.Apellido;
  var clave = req.body.Clave;
  var persona  = {id:id, nombre:nombre, apellido:apellido};
  var auth     = {id:id, perfil:"usuario", clave:clave};
  personas.push(persona);
  autenticacion.push(auth);
  return res.send({"status":"ok"});
});


app.delete('/personas/:id', function(req,res){
  if (req.autenticacion.perfil == 'admin') {
    var index = personas.findIndex( elem => ( elem.id == req.params.id ));
    delete personas[index];
    return res.send("deleted " + req.params.id);
  } else {
    res.sendStatus(401).send();
  }
});

let autenticacion = [
  {
     id: 5,
     perfil: "usuario",
     clave: "a"
  },
  {
     id: 1,
     perfil: "admin",
     clave: "123456"
  },
  {
     id: 2,
     perfil: "admin",
     clave: "123456"
  },
  {
     id: 3,
     perfil: "admin",
     clave: "123456"
  },
  {
     id: 4,
     perfil: "admin",
     clave: "123456"
  }
];


let personas = [
  {
     id: 5,
     nombre: 'a',
     apellido: 'Verde'
  },
  {
     id: 1,
     nombre: 'Unicornio',
     apellido: 'Verde'
  },
  {
     id: 2,
     nombre: 'Quimera',
     apellido: 'Roja'

  },
  {
     id: 3,
     nombre: 'Pony',
     apellido: 'Azul'
  },
  {
     id: 4,
     nombre: 'Sirena',
     apellido: 'Gris'
  }

];

let pagos = [
  { 
    id: 1,
    de: 1,
    a:  2,
    monto: 3
  }

];


app.listen(httpPort, () => console.log("api up and running"));
